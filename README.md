# README #

### What is it? ###

This is a Contao extension to create order forms.
You can use the contao form creator to create the basic forms like 'address form' to ask for address details.
With this extension you can then combine these forms wo a single multipage order form.

* It supports conditions to skip forms.
* The Mail text can be customized with contao simple Tokens.
* Shipping countries and costs can be added and countries can be used in a select field.
* It works with MetaModels where you can manage products.

It is custom made (e.g templates) and only tested for one case. Code needs improvements. Therefore it is no stable release.
It defenitly is not and will never be a complete Shop-System. Therefor you should use something like Isotope.

Makes use of Haste
https://github.com/codefog/contao-haste

and MultiColumnWizard
https://github.com/menatwork/MultiColumnWizard