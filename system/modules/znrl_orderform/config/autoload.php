<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @license LGPL-3.0+
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
	'Znrl',
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Classes
	'Znrl\ZnrlOrderform\OrderformDca'            => 'system/modules/znrl_orderform/classes/OrderformDca.php',

	// Models
	'Znrl\ZnrlOrderform\OrderformFormsModel'     => 'system/modules/znrl_orderform/models/OrderformFormsModel.php',
	'Znrl\ZnrlOrderform\OrderformModel'          => 'system/modules/znrl_orderform/models/OrderformModel.php',
	'Znrl\ZnrlOrderform\OrderformShippingModel'  => 'system/modules/znrl_orderform/models/OrderformShippingModel.php',

	// Modules
	'Znrl\ZnrlOrderform\ModuleCartZnrlOrderform' => 'system/modules/znrl_orderform/modules/ModuleCartZnrlOrderform.php',
	'Znrl\ZnrlOrderform\ModuleZnrlOrderform'     => 'system/modules/znrl_orderform/modules/ModuleZnrlOrderform.php',
));


/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
	'fe_znrl_orderform_cart'         => 'system/modules/znrl_orderform/templates',
	'fe_znrl_orderform_confirmation' => 'system/modules/znrl_orderform/templates',
	'fe_znrl_orderform_form'         => 'system/modules/znrl_orderform/templates',
	'mod_cart_znrl_orderform'        => 'system/modules/znrl_orderform/templates',
	'mod_znrl_orderform'             => 'system/modules/znrl_orderform/templates',
));
