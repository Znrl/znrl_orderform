<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   ZnrlIcsCal
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2015
 */


/**
 * BACK END MODULES
 *
 */

$GLOBALS['BE_MOD']['content']['znrl_orderform'] = array(
    'tables'     => array('tl_znrl_orderform','tl_znrl_orderform_forms', 'tl_znrl_orderform_shipping'),
    'icon'       => 'system/modules/znrl_orderform/assets/iconznrlorderform.png',
    'stylesheet' => 'system/modules/znrl_orderform/assets/znrl_orderform_be.css'
);



/**
 * Front end modules
 *
 */
$GLOBALS['FE_MOD']['znrl']['znrl_orderform'] = 'Znrl\ZnrlOrderform\ModuleZnrlOrderform';
$GLOBALS['FE_MOD']['znrl']['cart_znrl_orderform'] = 'Znrl\ZnrlOrderform\ModuleCartZnrlOrderform';



/**
 * MODEL MAPPINGS
 *
 */

$GLOBALS['TL_MODELS']['tl_znrl_orderform'] = 'Znrl\ZnrlOrderform\OrderformModel';
$GLOBALS['TL_MODELS']['tl_znrl_orderform_forms'] = 'Znrl\ZnrlOrderform\OrderformFormsModel';
$GLOBALS['TL_MODELS']['tl_znrl_orderform_shipping'] = 'Znrl\ZnrlOrderform\OrderformShippingModel';
