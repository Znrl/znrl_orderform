<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   ZnrlOrderform
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2015
 */


/**
 * Namespace
 */

namespace Znrl\ZnrlOrderform;


/**
 * Model ZnrlOrderformShipping
 *
 * @copyright  Lorenz Ketterer 2015
 * @author     Lorenz Ketterer <lorenz.ketterer@web.de>
 */


class OrderformShippingModel extends \Model
{
    protected static $strTable = 'tl_znrl_orderform_shipping';
}