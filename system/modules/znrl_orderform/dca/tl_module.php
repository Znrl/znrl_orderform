<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   ZnrlOrderform
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2014
 */


/**
 * Add palettes to tl_module
*/
$GLOBALS['TL_DCA']['tl_module']['palettes']['znrl_orderform']   = 'name,type,headline;{znrl_orderform_config_legend},znrl_orderform,znrl_orderform_template;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';
$GLOBALS['TL_DCA']['tl_module']['palettes']['cart_znrl_orderform']   = 'name,type,headline;{cart_znrl_orderform_config_legend},znrl_orderform,znrl_orderform_page,cart_znrl_orderform_show_always,cart_znrl_orderform_template;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';


/**
 * Add fields to tl_module
 */
$GLOBALS['TL_DCA']['tl_module']['fields']['znrl_orderform'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['znrl_orderform'],
    'exclude'                 => true,
    'inputType'               => 'select',
    'foreignKey'              => 'tl_znrl_orderform.title',
    'sql'                     => "varchar(255) NOT NULL default ''",
    'eval'                    => array('multiple'=>false, 'mandatory'=>true, 'tl_class'=>'w50')
);
$GLOBALS['TL_DCA']['tl_module']['fields']['znrl_orderform_template'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['znrl_orderform_template'],
    'default'                 => '',
    'exclude'                 => true,
    'inputType'               => 'select',
    'options_callback'        => array('Znrl\ZnrlOrderform\OrderformDca', 'getTemplates'),
    'sql'                     => "varchar(64) NOT NULL default ''",
    'eval'                    => array('tl_class'=>'w50')
);
$GLOBALS['TL_DCA']['tl_module']['fields']['znrl_orderform_page'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['znrl_orderform_page'],
    'exclude'                 => true,
    'inputType'               => 'pageTree',
    'foreignKey'              => 'tl_page.title',
    'eval'                    => array('fieldType'=>'radio', 'tl_class'=>'clr'),
    'sql'                     => "int(10) unsigned NOT NULL default '0'",
    'relation'                => array('type'=>'hasOne', 'load'=>'lazy')
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cart_znrl_orderform_show_always'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['cart_znrl_orderform_show_always'],
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'eval'                    => array('tl_class'=>'w50'),
    'sql'                     => "char(1) NOT NULL default ''"
);