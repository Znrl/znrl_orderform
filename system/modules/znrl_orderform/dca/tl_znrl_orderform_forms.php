<?php

use Haste\Form\Validator\MandatoryOn;
/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   ZnrlOrderform
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2014
 */


/**
 * Table tl_znrl_orderform_forms
 */
$GLOBALS['TL_DCA']['tl_znrl_orderform_forms'] = array
(

    // Config
    'config' => array
    (
        'dataContainer'               => 'Table',
        'ptable'                      => 'tl_znrl_orderform',
        'enableVersioning'            => true,
        'sql' => array
        (
            'keys' => array
            (
                'id'  => 'primary',
                'pid' => 'index'
            )
        )
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode'                    => 4,
            'fields'                  => array('sorting'),
            'panelLayout'             => 'filter,search,limit',
            'headerFields'            => array('title', 'tstamp', 'id'),
            'child_record_callback'   => array('Znrl\ZnrlOrderform\OrderformDca', 'listFormElements')
        ),
        'label' => array
        (
            'fields'                  => array('title'),
            'format'                  => '%s'
        ),
        'global_operations' => array
        (
            'all' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href'                => 'act=select',
                'class'               => 'header_edit_all',
                'attributes'          => 'onclick="Backend.getScrollOffset();" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['edit'],
                'href'                => 'act=edit',
                'icon'                => 'edit.gif'
            ),
            'copy' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['copy'],
                'href'                => 'act=copy',
                'icon'                => 'copy.gif'
            ),
            'cut' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['cut'],
                'href'                => 'act=paste&amp;mode=cut',
                'icon'                => 'cut.gif',
                'attributes'          => 'onclick="Backend.getScrollOffset()"'
            ),
            'delete' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['delete'],
                'href'                => 'act=delete',
                'icon'                => 'delete.gif',
                'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
            ),
            'show' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['show'],
                'href'                => 'act=show',
                'icon'                => 'show.gif'
            )
        )
    ),

    // Select
    'select' => array
    (
        'buttons_callback' => array()
    ),

    // Edit
    'edit' => array
    (
        'buttons_callback' => array()
    ),

    // Palettes
    'palettes' => array
    (
        '__selector__'                => array('type','form_condition'),
        'default'                     => '{title_legend},title;{type_legend},type',
        'form'                        => '{title_legend},title;{type_legend},type;{form_legend},form,form_condition,template',
        'cart'                        => '{title_legend},title;{type_legend},type;{form_legend},template',
        'confirmation'                => '{title_legend},title;{type_legend},type;{form_legend},form,form_condition,formdata_exclusion,redirect_page,template;{mail_legend},mail_recipient,mail_subject,mail_cart_text,mail_text'
    ),

    // Subpalettes
    'subpalettes' => array
    (
        'form_condition'               => 'form_condition_orderform,form_condition_field,form_condition_value,'
    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
        'pid' => array
        (
            'foreignKey'              => 'tl_znrl_orderform.title',
            'sql'                     => "int(10) unsigned NOT NULL default '0'",
            'relation'                => array('type'=>'belongsTo', 'load'=>'lazy')
        ),
        'sorting' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'tstamp' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'title' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['title'],
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>255),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'type' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['type'],
            'exclude'                 => true,
            'sorting'                 => true,
            'inputType'               => 'select',
            'options'                 => array('cart', 'form', 'confirmation'),
            'reference'               => &$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['type_select_options'],
            'eval'                    => array('mandatory' => true, 'multiple' => false, 'includeBlankOption' => true, 'submitOnChange'=> true),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'form' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['form'],
            'exclude'                 => true,
            'inputType'               => 'select',
            'options_callback'        => array('Znrl\ZnrlOrderform\OrderformDca', 'getForms'),
            'eval'                    => array('tl_class'=>'w50', 'multiple' => false, 'includeBlankOption' => false, 'submitOnChange'=> true),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'form_condition' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['form_condition'],
            'exclude'                 => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('tl_class'=>'w50 cbx m12','submitOnChange'=> true),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'form_condition_orderform' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['form_condition_orderform'],
            'exclude'                 => true,
            'inputType'               => 'select',
            'options_callback'        => array('Znrl\ZnrlOrderform\OrderformDca', 'getAllOtherOrderforms'),
            'eval'                    => array('tl_class'=>'w50', 'multiple' => false, 'includeBlankOption' => false, 'submitOnChange'=> true),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'form_condition_field' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['form_condition_field'],
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('tl_class'=>'w50 clr', 'mandatory'=>true, 'maxlength'=>255),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'form_condition_value' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['form_condition_value'],
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'decodeEntities'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'formdata_exclusion' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['formdata_exclusion'],
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('maxlength'=>255),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'redirect_page' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['redirect_page'],
            'exclude'                 => true,
            'inputType'               => 'pageTree',
            'foreignKey'              => 'tl_page.title',
            'eval'                    => array('fieldType'=>'radio', 'tl_class'=>'clr'),
            'sql'                     => "int(10) unsigned NOT NULL default '0'",
            'relation'                => array('type'=>'hasOne', 'load'=>'lazy')
        ),
        'template' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['template'],
            'exclude'                 => true,
            'inputType'               => 'select',
            'options_callback'        => array('Znrl\ZnrlOrderform\OrderformDca', 'getTemplates'),
            'eval'                    => array('tl_class'=>'clr', 'multiple' => false, 'includeBlankOption' => false, 'submitOnChange'=> true),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),

        'mail_recipient' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['mail_recipient'],
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>1022, 'rgxp'=>'emails', 'tl_class'=>'w50'),
            'sql'                     => "varchar(1022) NOT NULL default ''"
        ),
        'mail_subject' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['mail_subject'],
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'mail_cart_text' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['mail_cart_text'],
            'exclude'                 => true,
            'inputType'               => 'textarea',
            'eval'                    => array('decodeEntities'=>true, 'class'=>'noresize'),
            'sql'                     => "mediumtext NULL"
        ),
        'mail_text' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['mail_text'],
            'exclude'                 => true,
            'inputType'               => 'textarea',
            'eval'                    => array('decodeEntities'=>true, 'class'=>'noresize'),
            'sql'                     => "mediumtext NULL"
        ),
    )
);
