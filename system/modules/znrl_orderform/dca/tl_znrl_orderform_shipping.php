<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   ZnrlOrderform
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2014
 */


/**
 * Table tl_znrl_orderform_shipping
 */
$GLOBALS['TL_DCA']['tl_znrl_orderform_shipping'] = array
(

    // Config
    'config' => array
    (
        'dataContainer'               => 'Table',
        'enableVersioning'            => true,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary'
            )
        ),
        'backlink'                    => 'do=znrl_orderform'
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode'                    => 1,
            'fields'                  => array('title'),
            'flag'                    => 1,
            'panelLayout'             => 'filter;search,limit'
        ),
        'label' => array
        (
            'fields'                  => array('title'),
            'format'                  => '%s'
        ),
        'global_operations' => array
        (
            'all' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href'                => 'act=select',
                'class'               => 'header_edit_all',
                'attributes'          => 'onclick="Backend.getScrollOffset();" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_znrl_orderform_shipping']['edit'],
                'href'                => 'act=edit',
                'icon'                => 'edit.gif'
            ),
            'copy' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_znrl_orderform_shipping']['copy'],
                'href'                => 'act=copy',
                'icon'                => 'copy.gif'
            ),
            'delete' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_znrl_orderform_shipping']['delete'],
                'href'                => 'act=delete',
                'icon'                => 'delete.gif',
                'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
            ),
            'show' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_znrl_orderform_shipping']['show'],
                'href'                => 'act=show',
                'icon'                => 'show.gif'
            )
        )
    ),

    // Select
    'select' => array
    (
        'buttons_callback' => array()
    ),

    // Edit
    'edit' => array
    (
        'buttons_callback' => array()
    ),

    // Palettes
    'palettes' => array
    (
        '__selector__'                => array(''),
        'default'                     => '{title_legend},title;{shipping_options_legend},shipping_options'
    ),

    // Subpalettes
    'subpalettes' => array
    (
        ''                            => ''
    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
        'tstamp' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'title' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform_shipping']['title'],
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>255),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'shipping_options' => array
        (
            'label'			=> &$GLOBALS['TL_LANG']['tl_znrl_orderform_shipping']['shipping_options'],
            'exclude' 		=> true,
            'inputType'     => 'multiColumnWizard',
            'eval' 			=> array('columnFields' => array
                               (
                                    'country_value' => array
                                    (
                                       'label'     => &$GLOBALS['TL_LANG']['tl_znrl_orderform_shipping']['country_value'],
                                       'exclude'   => true,
                                       'inputType' => 'text',
                                       'eval' 	    => array('style' => 'width:200px')
                                    ),
                                    'country' => array
                                    (
                                        'label'     => &$GLOBALS['TL_LANG']['tl_znrl_orderform_shipping']['country'],
                                        'exclude'   => true,
                                        'inputType' => 'text',
                                        'eval' 	    => array('style' => 'width:200px', 'decodeEntities'=>true,)
                                    ),
                                    'shipping_charges' => array
                                    (
                                        'label'     => &$GLOBALS['TL_LANG']['tl_znrl_orderform_shipping']['shipping_charges'],
                                        'exclude'   => true,
                                        'inputType' => 'text',
                                        'eval' 	    => array('rgxp' => 'digit', 'style' => 'width:100px')
                                    ),
                                    'shipping_charges_business' => array
                                    (
                                        'label'     => &$GLOBALS['TL_LANG']['tl_znrl_orderform_shipping']['shipping_charges_business'],
                                        'exclude'   => true,
                                        'inputType' => 'text',
                                        'eval' 	    => array('rgxp' => 'digit', 'style' => 'width:100px')
                                    ),


                               )
            ),
            'sql'           => "text NOT NULL"
        )
    )
);
