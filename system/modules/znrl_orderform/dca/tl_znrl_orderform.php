<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   ZnrlOrderform
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2014
 */


/**
 * Table tl_znrl_orderform
 */
$GLOBALS['TL_DCA']['tl_znrl_orderform'] = array
(

    // Config
    'config' => array
    (
        'dataContainer'               => 'Table',
        'ctable'                      => array('tl_znrl_orderform_forms'),
        'enableVersioning'            => true,
        'switchToEdit'                => true,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary'
            )
        )
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode'                    => 2,
            'fields'                  => array('title'),
            'flag'                    => 1,
            'panelLayout' => 'filter;sort,search,limit'
        ),
        'label' => array
        (
            'fields'                  => array('title'),
            'format'                  => '%s'
        ),
        'global_operations' => array
        (
            'shipping' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['shipping'],
                'href'                => 'table=tl_znrl_orderform_shipping',
                'class'               => 'header_znrl_orderform_shipping',
                'attributes'          => 'onclick="Backend.getScrollOffset();"'
            ),
            'all' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href'                => 'act=select',
                'class'               => 'header_edit_all',
                'attributes'          => 'onclick="Backend.getScrollOffset();" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['edit'],
                'href'                => 'table=tl_znrl_orderform_forms',
                'icon'                => 'edit.gif'
            ),
            'editheader' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['editheader'],
                'href'                => 'act=edit',
                'icon'                => 'header.gif',
            ),
            'copy' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['copy'],
                'href'                => 'act=copy',
                'icon'                => 'copy.gif'
            ),
            'delete' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['delete'],
                'href'                => 'act=delete',
                'icon'                => 'delete.gif',
                'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
            ),
            'show' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['show'],
                'href'                => 'act=show',
                'icon'                => 'show.gif'
            )
        )
    ),

    // Select
    'select' => array
    (
        'buttons_callback' => array()
    ),

    // Edit
    'edit' => array
    (
        'buttons_callback' => array()
    ),

    // Palettes
    'palettes' => array
    (
        '__selector__'                => array('products','products_add_cols','shipping_settings','shipping_charges_use_global'),
        'default'                     => '{title_legend},title;{products_legend},products,products_page;{tax_legend},tax_rate;{shipping_settings_legend},shipping_settings,business_decider_field,business_decider_value;{currency_legend},currency;',
        'table'                       => '{title_legend},title;{products_legend},products,products_table,products_id_col,products_title_col,products_price_col,products_add_cols,products_page;{tax_legend},tax_rate;{shipping_settings_legend},shipping_settings,business_decider_field,business_decider_value;{currency_legend},currency;'
    ),

    // Subpalettes
    'subpalettes' => array
    (
        'products_add_cols'                                   => 'products_additional_cols',
        'shipping_settings_global'                            => 'shipping_charges_global,shipping_charges_global_business',
        'shipping_settings_znrl_orderform_shipping'           => 'shipping_options,shipping_options_field,shipping_charges_use_global',
        'shipping_charges_use_global'                         => 'shipping_charges_global,shipping_charges_global_business'
    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
        'tstamp' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'title' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['title'],
            'exclude'                 => true,
            'sorting'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory' => true, 'unique' => true, 'maxlength' => 255, 'tl_class' => 'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'products' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['products'],
            'exclude'                 => true,
            'sorting'                 => true,
            'inputType'               => 'select',
            'options'                 => array('table'),
            'reference'               => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['products_select_options'],
            'eval'                    => array('includeBlankOption' => true, 'multiple' => false, 'chosen'=>true, 'submitOnChange'=> true),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'products_table' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['products_table'],
            'default'                 => 'tl_article',
            'exclude'                 => true,
            'search'                  => true,
            'sorting'                 => true,
            'inputType'               => 'select',
            'options_callback'        => array('Znrl\ZnrlOrderform\OrderformDca', 'getTables'),
            'eval'                    => array('mandatory' => false, 'multiple' => false, 'chosen'=>true, 'submitOnChange'=> true),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'products_id_col' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['products_id_col'],
            'exclude'                 => true,
            'inputType'               => 'select',
            'options_callback'        => array('Znrl\ZnrlOrderform\OrderformDca', 'getCols'),
            'eval'                    => array('mandatory' => false, 'multiple' => false, 'chosen'=>true, 'submitOnChange'=> true),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'products_title_col' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['products_title_col'],
            'exclude'                 => true,
            'inputType'               => 'select',
            'options_callback'        => array('Znrl\ZnrlOrderform\OrderformDca', 'getCols'),
            'eval'                    => array('mandatory' => false, 'multiple' => false, 'chosen'=>true, 'submitOnChange'=> true),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'products_price_col' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['products_price_col'],
            'exclude'                 => true,
            'inputType'               => 'select',
            'options_callback'        => array('Znrl\ZnrlOrderform\OrderformDca', 'getCols'),
            'eval'                    => array('mandatory' => false, 'multiple' => false, 'chosen'=>true, 'submitOnChange'=> true),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'products_add_cols' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['products_add_cols'],
            'exclude'                 => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('tl_class'=>'w50 m12 cbx', 'submitOnChange'=> true),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'products_additional_cols' => array
        (
            'label'			=> &$GLOBALS['TL_LANG']['tl_znrl_orderform']['products_additional_cols'],
            'exclude' 		=> true,
            'inputType'     => 'multiColumnWizard',
            'eval' 			=> array('tl_class'=>'clr', 'columnFields' => array
                               (
                                    'products_additional_cols_col' => array
                                    (
                                       'label'     => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['products_additional_cols_col'],
                                       'exclude'   => true,
                                       'inputType' => 'select',
                                       'options_callback'        => array('Znrl\ZnrlOrderform\OrderformDca', 'getCols'),
                                       'eval' 	    => array('style' => 'width:200px', 'includeBlankOption' => false, 'chosen'=>true)
                                    ),
                               )
            ),
            'sql'           => "text NOT NULL"
        ),
        'products_page' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['products_page'],
            'exclude'                 => true,
            'inputType'               => 'pageTree',
            'foreignKey'              => 'tl_page.title',
            'eval'                    => array('fieldType'=>'radio', 'tl_class'=>'clr'),
            'sql'                     => "int(10) unsigned NOT NULL default '0'",
            'relation'                => array('type'=>'hasOne', 'load'=>'lazy')
        ),
        'tax_rate' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['tax_rate'],
            'exclude'                 => true,
            'sorting'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory' => true, 'maxlength' => 255, 'rgxp' => 'prcnt', 'tl_class' => 'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'shipping_settings' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['shipping_settings'],
            'exclude'                 => true,
            'sorting'                 => true,
            'inputType'               => 'select',
            'options'                 => array('global', 'znrl_orderform_shipping'),
            'reference'               => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['shipping_settings_select_options'],
            'eval'                    => array('includeBlankOption' => false, 'multiple' => false, 'chosen'=>true, 'submitOnChange'=> true),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'shipping_options' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['shipping_options'],
            'exclude'                 => true,
            'search'                  => true,
            'sorting'                 => true,
            'inputType'               => 'select',
            'options_callback'        => array('Znrl\ZnrlOrderform\OrderformDca', 'getShippingOptions'),
            'eval'                    => array('mandatory' => true, 'multiple' => false, 'chosen'=>true, 'submitOnChange'=> true),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'shipping_options_field' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['shipping_options_field'],
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('tl_class'=>'w50 clr', 'mandatory'=>true, 'maxlength'=>255),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'shipping_charges_use_global' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['shipping_charges_use_global'],
            'exclude'                 => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('tl_class'=>'w50 m12 cbx', 'submitOnChange'=> true),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'shipping_charges_global' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['shipping_charges_global'],
            'exclude'                 => true,
            'sorting'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('maxlength' => 255, 'rgxp' => 'digit', 'tl_class' => 'clr w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'shipping_charges_global_business' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['shipping_charges_global_business'],
            'exclude'                 => true,
            'sorting'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('maxlength' => 255, 'rgxp' => 'digit', 'tl_class' => 'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'business_decider_field' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['business_decider_field'],
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('maxlength'=>255, 'tl_class'=>'clr w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'business_decider_value' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['business_decider_value'],
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('maxlength'=>255, 'decodeEntities'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'currency' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_znrl_orderform']['currency'],
            'exclude'                 => true,
            'sorting'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),

    )
);