<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   ZnrlIcsCal
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2015
 */


/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD'][''] = array('', '');


/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD'][''] = array('', '');
