<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   ZnrlOrderform
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2014
 */



/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['title_legend'] = 'Titel';
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['type_legend'] = 'Formulartyp';
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['form_legend'] = 'Formulareinstellungen';
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['mail_legend'] = 'Maileinstellungen';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['title'] = array('Titel', 'Bitte geben Sie einen Titel ein.');
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['type'] = array('Typ', 'Bitte wählen Sie die Art des Formulars.');
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['type_select_options']['cart'] = 'Einkaufswagen';
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['type_select_options']['form'] = 'Formular - Formulargenerator';
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['type_select_options']['confirmation'] = 'Bestätigung - Übersicht';
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['form'] = array('Formular', 'Bitte wählen Sie ein im Formulargenerator erstelltes Formular.');
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['form_redirect_page'] = array('Formular - Weiterleitungsseite', 'Bitte wählen Sie die Seite zu der, nach dem Absenden des Formulars, weitergeleitet werden soll.');
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['form_condition'] = array('Formular - Bedingung', 'Wählen Sie ob die Anzeige des Formulars an eine Bedingung geknüpft sein soll.');
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['form_condition_orderform'] = array('Orderform Formular - Bedingung', 'Wählen Sie, in welchem Orderform Formular das Feld für die Bedinung vorkommt.');
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['form_condition_field'] = array('Bedingung - Feld', 'Geben Sie den Feldnamen des Feldes für die Bedingung ein.');
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['form_condition_value'] = array('Bedingung - Wert', 'Wenn das Feld diesen Wert hat wird das Formular angezeigt.');
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['formdata_exclusion'] = array('Formulardaten ausschließen', 'Kommagetrennte Liste von Formularfeldnamen, die nicht in der Übersicht und E-Mail angezeiget werden sollen.');
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['redirect_page'] = array('Weiterleitungsseite', 'Bitte wählen Sie eine Seite, zu der nach erfolgreichem Versenden des Formulars weitergeleitet werden soll.');
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['template'] = array('Template', 'Bitte wählen Sie ein Template.');
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['mail_recipient'] = array('E-Mail Adresse Empfänger', 'Abgeschickte Formulare werden an diese Adresse geschickt (und eine Kopie an die im Formularfeld "email" eingetragene Adresse).');
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['mail_subject'] = array('E-Mail Betreff', 'Wird als Betreff in der E-Mail angegeben.');
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['mail_cart_text'] = array('E-Mail "Cart" Text', 'Dieser Text wird für jedes Produkt im Einkaufswagen im E-Mail Text für ##mail_cart_text## eingefügt.');
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['mail_text'] = array('E-Mail Text', 'Gestalten Sie hier den Text der E-Mail.');

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['new']    = array('Neu', 'Formular hinzufügen');
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['show']   = array('Details', 'Details von ID %s anzeigen');
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['edit']   = array('Bearbeiten ', 'Bearbeite ID %s');
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['cut']    = array('Verschieben ', 'Verschiebe ID %s');
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['copy']   = array('Duplizieren ', 'Dupliziere ID %s');
$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['delete'] = array('Löschen ', 'Lösche ID %s');