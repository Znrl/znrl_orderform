<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   ZnrlIcsCal
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2015
 */


/**
 * Miscellaneous
 */
$GLOBALS['TL_LANG']['MSC']['znrl_orderform']['sum_plus'] = 'zzgl.';
$GLOBALS['TL_LANG']['MSC']['znrl_orderform']['inclusive_of'] = 'inkl.';
$GLOBALS['TL_LANG']['MSC']['znrl_orderform']['value_added_tax'] = 'Mehrwertsteuer';
$GLOBALS['TL_LANG']['MSC']['znrl_orderform']['shipping_charge'] = 'Versandkosten';
$GLOBALS['TL_LANG']['MSC']['znrl_orderform']['shipping_charge_free'] = 'Versandkostenfrei';
$GLOBALS['TL_LANG']['MSC']['znrl_orderform']['business'] = 'gewerblich';
$GLOBALS['TL_LANG']['MSC']['znrl_orderform']['cart_empty'] = 'Es befinden sich keine Artikel im Warenkorb.';
$GLOBALS['TL_LANG']['MSC']['znrl_orderform']['cart_continue_shopping'] = 'weiter einkaufen';
$GLOBALS['TL_LANG']['MSC']['znrl_orderform']['cart_update'] = 'Warenkorb aktualisieren';
$GLOBALS['TL_LANG']['MSC']['znrl_orderform']['cart_checkout'] = 'zur Kasse';
$GLOBALS['TL_LANG']['MSC']['znrl_orderform']['checkout_continue'] = 'weiter';
$GLOBALS['TL_LANG']['MSC']['znrl_orderform']['checkout_back'] = 'zurück';
$GLOBALS['TL_LANG']['MSC']['znrl_orderform']['checkout_change'] = 'ändern';
$GLOBALS['TL_LANG']['MSC']['znrl_orderform']['purchase_submit'] = 'kaufen';
