<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   ZnrlOrderform
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2014
 */



/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_znrl_orderform_shipping']['title_legend'] = 'Titel';
$GLOBALS['TL_LANG']['tl_znrl_orderform_shipping']['shipping_options_legend'] = 'Versandoptionen und Einstellungen';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_znrl_orderform_shipping']['title'] = array('Titel', 'Bitte geben Sie einen Titel ein.');
$GLOBALS['TL_LANG']['tl_znrl_orderform_shipping']['shipping_options'] = array('Versandoptionen', 'Die Länder können im Bestellformular zur Auswahl angeboten werden. Versandkosten: Geben Sie "0" für keine Versandkosten ein, ein leeres Feld wird als "exklusive Versandkosten" gewertet.');
$GLOBALS['TL_LANG']['tl_znrl_orderform_shipping']['country_value'] = array('Land - Feldname', 'Der Wert, der im Formular zur Identifikation genutzt wird.');
$GLOBALS['TL_LANG']['tl_znrl_orderform_shipping']['country'] = array('Land - Feldbezeichnung', 'Bitte geben Sie den Namen des Landes ein (Label). Fügen sie alle Länder hinzu, die im Bestellformular zur Auswahl angeboten werden sollen.');
$GLOBALS['TL_LANG']['tl_znrl_orderform_shipping']['shipping_charges'] = array('Versandkosten', 'Bitte geben Sie die Versandkosten OHNE Währungszeichen ein.');
$GLOBALS['TL_LANG']['tl_znrl_orderform_shipping']['shipping_charges_business'] = array('Versandkosten - Business', 'Bitte geben Sie die Versandkosten OHNE Währungszeichen ein.');

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_znrl_orderform_shipping']['new']    = array('Neu', 'Versandeinstellungen anlegen');
$GLOBALS['TL_LANG']['tl_znrl_orderform_shipping']['show']   = array('Details', 'Details von ID %s anzeigen');
$GLOBALS['TL_LANG']['tl_znrl_orderform_shipping']['edit']   = array('Bearbeiten ', 'Bearbeite ID %s');
$GLOBALS['TL_LANG']['tl_znrl_orderform_shipping']['cut']    = array('Verschieben ', 'Verschiebe ID %s');
$GLOBALS['TL_LANG']['tl_znrl_orderform_shipping']['copy']   = array('Duplizieren ', 'Dupliziere ID %s');
$GLOBALS['TL_LANG']['tl_znrl_orderform_shipping']['delete'] = array('Löschen ', 'Lösche ID %s');