<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   ZnrlIcsCal
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2015
 */


/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['znrl_orderform'] = array('Bestellformulare', 'Bestellformulare erstellen und Verwalten');
$GLOBALS['TL_LANG']['MOD']['tl_znrl_orderform_shipping'] = 'Versand';


/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['znrl'] = 'Znrl - Module';
$GLOBALS['TL_LANG']['FMD']['znrl_orderform'] = array('Bestellformular', 'Mit diesem Modul kann aus meheren Formularen ein erweitertes Bestellformular generiert werden.');
$GLOBALS['TL_LANG']['FMD']['cart_znrl_orderform'] = array('Einkaufswagen - Bestellformular', 'Mit diesem Modul kann im frontend ein Einkaufwaen mit der Anzahl an Artikeln im Einkaufwagen angezeigt werden.');
