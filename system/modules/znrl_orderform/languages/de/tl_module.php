<?php

/**
 * Contao Open Source CMS
*
* Copyright (c) 2005-2014 Leo Feyer
*
* @package   ZnrlOrderform
* @author    Lorenz Ketterer <lorenz.ketterer@web.de>
* @license   GNU/LGPL
* @copyright Lorenz Ketterer 2014
*/



/**
 * Legends
*/
$GLOBALS['TL_LANG']['tl_module']['znrl_orderform_config_legend'] = 'Bestellformular-Einstellungen';
$GLOBALS['TL_LANG']['tl_module']['cart_znrl_orderform_config_legend'] = 'Einkaufswagen-Einstellungen';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_module']['znrl_orderform'] = array('Bestellformular', 'Bitte wählen Sie ein Bestellformular.');
$GLOBALS['TL_LANG']['tl_module']['znrl_orderform_template'] = array('Template', 'Bitte wählen Sie ein Template.');
$GLOBALS['TL_LANG']['tl_module']['znrl_orderform_page'] = array('Bestellformular - Seite', 'Bitte wählen Sie die Seite auf er das Bestellformular eingebuden ist (als Link für den Warenkorb).');
$GLOBALS['TL_LANG']['tl_module']['cart_znrl_orderform_show_always'] = array('Immer anzeigen', 'Bitte wählen Sie ob der Einkaufskorb immer angezeigt werden soll, oder nur wenn sich Artikel darin befinden.');
