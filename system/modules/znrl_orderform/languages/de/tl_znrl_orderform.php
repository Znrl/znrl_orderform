<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   ZnrlOrderform
 * @author    Lorenz Ketterer <lorenz.ketterer@web.de>
 * @license   GNU/LGPL
 * @copyright Lorenz Ketterer 2014
 */



/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_znrl_orderform']['title_legend'] = 'Titel';
$GLOBALS['TL_LANG']['tl_znrl_orderform']['products_legend'] = 'Produkte';
$GLOBALS['TL_LANG']['tl_znrl_orderform']['tax_legend'] = 'Steuern';
$GLOBALS['TL_LANG']['tl_znrl_orderform']['shipping_settings_legend'] = 'Versandeinstellungen';
$GLOBALS['TL_LANG']['tl_znrl_orderform']['currency_legend'] = 'Währungseinstellungen';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_znrl_orderform']['title'] = array('Titel', 'Bitte geben Sie einen Titel ein.');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['products'] = array('Produkte', 'Bitte wählen Sie, woher die Produktdaten bezogen werden sollen.');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['products_select_options']['table'] = 'Tabelle';
$GLOBALS['TL_LANG']['tl_znrl_orderform']['products_table'] = array('Produkte - Tabelle', 'Bitte wählen Sie die Tabelle, in der die Produktdaten gespeichert sind.');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['products_title_col'] = array('Produkte - Titel', 'Bitte wählen Sie die Spalte mit dem Produkttitel.');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['products_id_col'] = array('Produkte - Artikelnummer', 'Bitte wählen Sie die Spalte mit der Artikelnummer. Dies kann auch die ID sein. Bestimmt wie der Link zur bestelleung aussen muss: addarticle=?');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['products_price_col'] = array('Produkte - Preis', 'Bitte wählen Sie die Spalte mit dem Produktpreis.');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['products_add_cols'] = array('Produkte - Weitere Felder hinzufügen', 'Fügen Sie alle Spalten hinzu, die sie in Frontend oder E-Mail verwenden wollen.');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['products_additional_cols'] = array('Produkte - Zusätzliche Felder', 'Wählen Sie hier zusätzliche Spalten, die im Frontend und der E-Mail zur Verfügung stehen sollen.');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['products_additional_cols_col'] = array('Spalte', 'Die Spalte aus der oben angegeben Tabelle.');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['products_page'] = array('Produkte - Seite', 'Bitte wählen Sie die Produktauswahlseite, als Ziel für den - Weiter Einkaufen - Link.');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['tax_rate'] = array('Steuersatz', 'Bitte geben Sie den Steuersatz in Prozent OHNE %-Zeichen ein.');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['shipping_settings'] = array('Versandeinstellungen', 'Bitte wählen Sie eine Versandeinstellung. Globale Einstellungen werden nachfolgend definiert.');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['shipping_settings_select_options']['global'] = 'Globale Einstellungen';
$GLOBALS['TL_LANG']['tl_znrl_orderform']['shipping_settings_select_options']['znrl_orderform_shipping'] = 'Eigene Versandeinstellung';
$GLOBALS['TL_LANG']['tl_znrl_orderform']['shipping_options'] = array('Versandoptionen', 'Bitte wählen Sie eine unter "Versand" angelgte Versandeinstellung. Diese können als Optionen für eine Länderauswahl im Formular genutzt werden.');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['shipping_options_field'] = array('Feld - Versandoptionen', 'Wählen Sie hier den Feldnamen, bei dem die Länder als Optionen eingefügt werden sollen. (Für die Versandkosten wird das letzte nicht ausgelassene Feld genommen.)');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['shipping_charges_global'] = array('Globale Versandkosten', 'Bitte geben Sie die Versandkosten ohne Währungszeichen ein. Geben Sie "0" für keine Versandkosten ein, ein leeres Feld wird als "exklusive Versandkosten" gewertet.');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['shipping_charges_global_business'] = array('Globale Versandkosten - Business', 'Bitte geben Sie die Versandkosten für geschäftliche Käufer ohne Währungszeichen ein. Geben Sie "0" für keine Versandkosten ein, ein leeres Feld wird als "exklusive Versandkosten" gewertet.');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['shipping_charges_use_global'] = array('Globale Versandkosten Verwenden', 'Mit dieser Einstellung werden lediglich die Länder aus den Versandeinstellungen benutzt.');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['business_decider_field'] = array('Business - Field', 'Geben Sie hier den Feldnamen ein über den bestimmt wird, ob es sich um eine geschäfliche Bestellung handelt.');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['business_decider_value'] = array('Business - Value', 'Geben Sie hier den Wert ein, bei dem die Bestellung als geschäftlich gehandhabt werden soll. #ANY# - bedeutet das Feld muss lediglich (beliebiger Wert) ausgefüllt werden.');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['currency'] = array('Währung', 'Bitte geben das Währungszeichen oder die Bezeichnung an, die hinter Preisangaben stehen soll (Bspw.: € oder EUR).');

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_znrl_orderform']['shipping'] = array('Versand', 'Versandeinstellungen - Länder und Kosten - verwalten');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['new']    = array('Neu', 'Bestellformular anlegen');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['show']   = array('Details', 'Details von ID %s anzeigen');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['edit']   = array('Bearbeiten ', 'Bearbeite ID %s');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['editheader']   = array('Einstellungen Bearbeiten ', 'Bearbeite die Einstellungen von ID %s');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['cut']    = array('Verschieben ', 'Verschiebe ID %s');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['copy']   = array('Duplizieren ', 'Dupliziere ID %s');
$GLOBALS['TL_LANG']['tl_znrl_orderform']['delete'] = array('Löschen ', 'Lösche ID %s');