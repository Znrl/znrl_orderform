<?php

/**
 * Contao Open Source CMS
*
* Copyright (c) 2005-2014 Leo Feyer
*
* @package   ZnrlOrderform
* @author    Lorenz Ketterer <lorenz.ketterer@web.de>
* @license   GNU/LGPL
* @copyright Lorenz Ketterer 2015
*/


/**
 * Namespace
*/
namespace Znrl\ZnrlOrderform;
use Contao\Database;

/**
 * Class OrderformDca
 *
 * Helper Class for DCA
 * @copyright  Lorenz Ketterer 2015
 * @author     Lorenz Ketterer <lorenz.ketterer@web.de>
 */

class OrderformDca extends \Backend
{
    public $productsTable;

    public $colNames;

    /**
     * Gets all Tables for options_callback to choose where the products are saved
     * @return array
     */
    public function getTables()
    {
        return $this->Database->listTables();
    }


    public function getCols($dc)
    {
        if (empty($this->productsTable) || $this->productsTable != $dc->activeRecord->products_table) {
            if (!isset($dc->activeRecord->products_table) || empty($dc->activeRecord->products_table)) {
                $this->productsTable = 'tl_article';
            }
            elseif (!empty($dc->activeRecord->products_table) || $this->productsTable != $dc->activeRecord->products_table) {
                $this->productsTable = $dc->activeRecord->products_table;
            }

            $allFields = $this->Database->listFields($this->productsTable);

            foreach ($allFields as $field) {
                $this->colNames[] = $field['name'];
            }
        }

        return $this->colNames;
    }


    public function getShippingOptions()
    {
        $shippingOptions = OrderformShippingModel::findAll();

        foreach ($shippingOptions as $shippingOption) {
            $shippingOptionTitles[$shippingOption->id] = $shippingOption->title;
        }

        return $shippingOptionTitles;
    }


    public function getForms()
    {
        $forms = \FormModel::findAll();

        foreach ($forms as $form) {
            $formTitles[$form->id] = $form->title;
        }

        return $formTitles;
    }


    public function listFormElements($arrRow)
    {
        $element =  $arrRow['title'].' ('.$GLOBALS['TL_LANG']['tl_znrl_orderform_forms']['type_select_options'][$arrRow['type']].')';

        return $element;
    }


    public function getTemplates($dc)
    {
        switch ($dc->activeRecord->type) {
            case 'znrl_orderform':
                return $this->getTemplateGroup('mod_znrl_orderform');

            case 'znrl_cart_orderform':
                return $this->getTemplateGroup('mod_cart_znrl_orderform');

            case 'form':
                return $this->getTemplateGroup('fe_znrl_orderform_form');

            case 'cart':
                return $this->getTemplateGroup('fe_znrl_orderform_cart');

            case 'confirmation':
                return $this->getTemplateGroup('fe_znrl_orderform_confirmation');
        }
    }


    public function getAllOtherOrderforms($dc)
    {
        $forms = OrderformFormsModel::findAll();

        foreach ($forms as $form) {
            if ($form->id != $dc->id) {
            $formTitles[$form->id] = $form->title;
            }
        }

        return $formTitles;
    }
}