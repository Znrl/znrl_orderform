<?php

/**
 * Contao Open Source CMS
*
* Copyright (c) 2005-2014 Leo Feyer
*
* @package   ZnrlOrderform
* @author    Lorenz Ketterer <lorenz.ketterer@web.de>
* @license   GNU/LGPL
* @copyright Lorenz Ketterer 2015
*/


/**
 * Namespace
*/

namespace Znrl\ZnrlOrderform;
use Haste\Form\Form;
use Haste\Util;
use Haste\Generator;
use Contao;


/**
 * Module ZnrlOrderform
 *
 * @copyright  Lorenz Ketterer 2015
 * @author     Lorenz Ketterer <lorenz.ketterer@web.de>
 */

class ModuleZnrlOrderform extends \Module
{

    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'mod_znrl_orderform';

    protected $arrOrderformSession = array();

    protected $arrOrderformSessionKey;

    protected $orderformSettings;


    /**
     * Display a wildcard in the back end
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new \BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### ' . utf8_strtoupper($GLOBALS['TL_LANG']['FMD']['znrl_orderform'][0]) . ' ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }

        return parent::generate();
    }


    /**
     * Generate the module
     */
    protected function compile()
    {

        // fraglich ob alles in der module.php sein sollte oder nicht was in ner eigenen Klasse bspw. Produkte
        //generell == oder === bei decidern und bedingungen prüfen wegen  null, 0 und '' auch bei = '' oder = null
        $this->arrOrderformSessionKey = 'znrl_orderform_'.$this->znrl_orderform;
        $this->getSessionData();


        if (\Input::get('addarticle') != null) {
            $this->addArticleAfterRedirect(\Input::get('addarticle'));
        }
        elseif (\Input::post('addarticle') != null) {
            $this->addArticleAfterRedirect(\Input::post('addarticle'));
        }


        if (is_numeric(\Input::get('gotostep'))) {
            $gotostep = \Input::get('gotostep');
        }
        elseif (is_numeric(\Input::post('gotostep'))) {
            $gotostep = \Input::post('gotostep');
        }
        elseif ((\Input::get('gotostep') != null) && \Input::get('gotostep') == 'cart') {
            $gotostep = $this->arrOrderformSession['stepCart'];
        }
        if (isset($gotostep) && $this->arrOrderformSession['forms'][$gotostep]['completed'] == true && $this->arrOrderformSession['forms'][$gotostep]['skip'] != 1 && \Input::post('FORM_SUBMIT') == null){
            $this->arrOrderformSession['step'] = (int) $gotostep;
            $this->setSessionData();
            $uri = \Haste\Util\Url::removeQueryString(array('gotostep'));
            \Controller::redirect($uri);
        }



        $this->orderformSettings = $this->getOrderformSettings();

        if (isset($this->arrOrderformSession['addarticle'])) {
            $this->addArticleToCart($this->arrOrderformSession['addarticle']);
            $this->arrOrderformSession['step'] = $this->arrOrderformSession['stepCart'];
        }

        if( $this->strTemplate != $this->znrl_orderform_template && $this->znrl_orderform_template != ''){

            $this->Template = new \FrontendTemplate($this->znrl_orderform_template);
        }

        $this->Template->orderformSettings = $this->orderformSettings;


        switch ($this->compileForm()) {
            case 'next':
                $this->arrOrderformSession['forms'][$this->arrOrderformSession['step']]['completed'] = 'true';
                if ($this->arrOrderformSession['step'] == $this->arrOrderformSession['stepCount'] - 1) {

                    break;
                }
                $this->arrOrderformSession['step']++;
                $this->setSessionData();
                $uri = \Environment::get('request');
                \Controller::redirect($uri);
                break;

            case 'skip':
                if ($this->arrOrderformSession['step'] >= $this->arrOrderformSession['stepCount'] - 1) {

                    break;
                }
                $this->arrOrderformSession['step']++;
                $this->setSessionData();
                $uri = \Environment::get('request');
                \Controller::redirect($uri);
                break;

            case 'send_orderform_failed':
                // vtl. unnötig bzw was machen?
                break;

            case 'update':
                $this->setSessionData();
                $uri = \Environment::get('request');
                \Controller::redirect($uri);
                break;

            case 'continue_shopping':
                if (($objPage =\PageModel::findByPk($this->orderformSettings->products_page)) != null) {
                    $this->setSessionData();
                    $uri = \Controller::generateFrontendUrl($objPage->row());
                    \Controller::redirect($uri);
                }
                break;

            case 'back':
                $this->arrOrderformSession['forms'][$this->arrOrderformSession['step']]['completed'] = 'true';
                $this->arrOrderformSession['step']--;
                if ($this->arrOrderformSession['forms'][$this->arrOrderformSession['step']]['skip'] == 1) {
                    $this->arrOrderformSession['step']--;
                }
                $this->setSessionData();
                $uri = \Environment::get('request');
                \Controller::redirect($uri);
                break;

            case 'invalid':
                $this->arrOrderformSession['forms'][$this->arrOrderformSession['step']]['completed'] = 'false';
                break;

        }

        $this->setSessionData();

    }


    protected function compileForm()
    {
        $currentOrderformForm = $this->getOrderformForm($this->arrOrderformSession['step']);


        $strTemplate = $currentOrderformForm->template;

        $formTemplate = new \FrontendTemplate($strTemplate);
        $formTemplate->currentForm = $currentOrderformForm;

        $submitFields = array();

        if ($currentOrderformForm->type == 'cart') {

            if (!isset($this->arrOrderformSession['formdata']['cart'])) {
                $this->arrOrderformSession['formdata']['cart'] = '';
            }


            $objForm = new \Haste\Form\Form('znrl_orderform_cart_'.$currentOrderformForm->id, 'POST', function($objHaste) {
                return \Input::post('FORM_SUBMIT') === $objHaste->getFormId();
            }, true);

            $objForm->addContaoHiddenFields();

            $objForm->addFormField('cart_update', array(
                'label'         => array($GLOBALS['TL_LANG']['MSC']['znrl_orderform']['cart_update'],''),
                'inputType'     => 'submit',
                'eval'          => array('class'=> 'cart_update')
            ));
            $objForm->addSubmitFormField('cart_continue_shopping', $GLOBALS['TL_LANG']['MSC']['znrl_orderform']['cart_continue_shopping']);

            $cartDetails = $this->getCartDetails();

            if ($cartDetails->cartIsEmpty != 1) {

                $objForm->addSubmitFormField('cart_checkout', $GLOBALS['TL_LANG']['MSC']['znrl_orderform']['cart_checkout']);

                $objProductDetails = $cartDetails->productDetails;

                foreach ($objProductDetails as $arrProductDetails) {

                   $objForm->addFormField('quantity_'.$arrProductDetails->{$this->orderformSettings->products_id_col}, array(
                        'label'         => array('x',''),
                        'inputType'     => 'text',
                        'value'         => $this->arrOrderformSession['formdata']['cart']['quantity_'.$arrProductDetails->{$this->orderformSettings->products_id_col}],
                        'eval'          => array('mandatory'=>false, 'rgxp'=>'digit', 'min' => '0')
                    ));
                   $arrProductDetails->orderformFormField = $objForm->getWidget('quantity_'.$arrProductDetails->{$this->orderformSettings->products_id_col})->parse();
                   $arrProductDetails->productQuantity = $this->arrOrderformSession['formdata']['cart']['quantity_'.$arrProductDetails->{$this->orderformSettings->products_id_col}];

                   $price = str_replace(",",".",$arrProductDetails->{$this->orderformSettings->products_price_col});

                   $productPriceSum = $arrProductDetails->productQuantity * $price;

                   $cartSumCost += $productPriceSum;

                   $arrProductDetails->{$this->orderformSettings->products_price_col} =  number_format($price,2,',','');
                   $arrProductDetails->productPriceSum = number_format($productPriceSum,2,',','');

                }
                $cartDetails->cartSumCost = number_format($cartSumCost,2,',','');
            }
            else {
                $cartDetails->cartIsEmptyText = $GLOBALS['TL_LANG']['MSC']['znrl_orderform']['cart_empty'];
            }


            $objWidgets = $objForm->getWidgets();

            foreach ($objWidgets as $widget) {
                if ($widget->type == 'submit') {
                    $submitFields[] = $widget->parse();
                }
            }

            $formTemplate->cartDetails = $cartDetails;

            $formTemplate->submitFields = $submitFields;


            if($objForm->validate()) {


                $objForm->addToTemplate($formTemplate);
                $this->Template->form = $formTemplate->parse();

                $objForm->fetchAll(function($strName, $objWidget) {
                    if ($objWidget->type != 'submit' && $objWidget->type != 'hidden' && $objWidget->type != 'fieldset' && $objWidget->type != 'explanation') {
                        if ($objWidget->value > 0) {
                            $this->arrOrderformSession['formdata']['cart'][$strName] = $objWidget->value;
                        }
                        else {
                            unset($this->arrOrderformSession['formdata']['cart'][$strName]);
                        }
                    }
                });



                if (\Input::post('cart_checkout') != null && $cartDetails->cartIsEmpty != 1) {

                    return 'next';
                }
                elseif (\Input::post('cart_continue_shopping') != null) {

                    return 'continue_shopping';
                }
                else {
                    return 'update';
                }


            }

        }
        elseif ($currentOrderformForm->type == 'form') {

            if ($currentOrderformForm->form_condition == 1) {
                // form conditions funktionieren vermutl nicht bei feldern mit array ? das sieht auch nicht elegant aus
                $i = 0;
                foreach ($this->arrOrderformSession['forms'] as $sessionForm) {
                    if ($sessionForm['id'] == $currentOrderformForm->form_condition_orderform && isset($this->arrOrderformSession['formdata'][$i][$currentOrderformForm->form_condition_field])) {
                        if ($this->arrOrderformSession['formdata'][$i][$currentOrderformForm->form_condition_field] != $currentOrderformForm->form_condition_value) {
                            $this->arrOrderformSession['forms'][$this->arrOrderformSession['step']]['skip'] = 1;

                            return 'skip';
                        }
                        else {
                            $this->arrOrderformSession['forms'][$this->arrOrderformSession['step']]['skip'] = '';
                        }
                    }
                    $i++;
                }
            }

            $objForm = new \Haste\Form\Form('znrl_orderform_form_'.$currentOrderformForm->id, 'POST', function($objHaste) {
                return \Input::post('FORM_SUBMIT') === $objHaste->getFormId();
            }, true);


            $objForm->addFieldsFromFormGenerator($currentOrderformForm->form, function(&$strField, &$arrDca) {

                if ($this->orderformSettings->shipping_settings == 'znrl_orderform_shipping' && $strField == $this->orderformSettings->shipping_options_field) {
                    $countries = $this->getShippingCoutries($this->orderformSettings->shipping_options);
                    $arrDca['options'] = $countries;
                }
                if (isset($this->arrOrderformSession['formdata'][$this->arrOrderformSession['step']][$strField])) {
                    $arrDca['value'] = $this->arrOrderformSession['formdata'][$this->arrOrderformSession['step']][$strField];
                }

                return true;
            });

            $objForm->addFormField('fieldset_submit_start', array(
                        'label'         => array('Fortfahren',''),
                        'inputType'     => 'fieldset',
                        'eval'          => array('fsType' => 'fsStart')
            ));
            $objForm->addSubmitFormField('checkout_back', $GLOBALS['TL_LANG']['MSC']['znrl_orderform']['checkout_back']);
            $objForm->addSubmitFormField('checkout_continue', $GLOBALS['TL_LANG']['MSC']['znrl_orderform']['checkout_continue']);
            $objForm->addFormField('fieldset_submit_end', array(
                        'inputType'     => 'fieldset',
                        'eval'          => array('fsType' => 'fsEnd')
            ));


            if($objForm->validate()) {


                $objForm->fetchAll(function($strName, $objWidget) {
                    //gibts noch mehre leere feld typen? vtl in array und dagegen checken
                    if ($objWidget->type != 'submit' && $objWidget->type != 'hidden' && $objWidget->type != 'fieldset' && $objWidget->type != 'explanation') {

                            $this->arrOrderformSession['formdata'][$this->arrOrderformSession['step']][$strName] = $objWidget->value;
                    }
                });


                if (\Input::post('checkout_back') != null) {
                    return 'back';
                }
                else {
                    return 'next';
                }

            }
        }
        elseif ($currentOrderformForm->type == 'confirmation') {

            if ($currentOrderformForm->form_condition == 1) {
                $i = 0;
                foreach ($this->arrOrderformSession['forms'] as $sessionForm) {
                    if ($sessionForm['id'] == $currentOrderformForm->form_condition_orderform && isset($this->arrOrderformSession['formdata'][$i][$currentOrderformForm->form_condition_field])) {
                        if ($this->arrOrderformSession['formdata'][$i][$currentOrderformForm->form_condition_field] != $currentOrderformForm->form_condition_value) {
                            $this->arrOrderformSession['forms'][$this->arrOrderformSession['step']]['skip'] = 1;

                            return 'skip';
                        }
                        else {
                            $this->arrOrderformSession['forms'][$this->arrOrderformSession['step']]['skip'] = '';
                        }
                    }
                    $i++;
                }
            }


            $objFormDataFromSession = $this->getPreparedFormDataFromSession();

            $cartDetails = $this->getCartDetails($objFormDataFromSession);

            $objProductDetails = $cartDetails->productDetails;

            foreach ($objProductDetails as $arrProductDetails) {
                // das auch in  getcartdetails machen (auch bei cart, außer widget)

                $arrProductDetails->productQuantity = $this->arrOrderformSession['formdata']['cart']['quantity_'.$arrProductDetails->{$this->orderformSettings->products_id_col}];

                $price = str_replace(",",".",$arrProductDetails->{$this->orderformSettings->products_price_col});

                $productPriceSum = $arrProductDetails->productQuantity * $price;

                $cartSumCost += $productPriceSum;

                $arrProductDetails->{$this->orderformSettings->products_price_col} =  number_format($price,2,',','');
                $arrProductDetails->productPriceSum = number_format($productPriceSum,2,',','');

                }
                $cartDetails->cartSumCost = str_replace(".",",",number_format($cartSumCost,2));

            $cartSumCost += str_replace(",",".",$cartDetails->shippingCharges);

            $cartDetails->cartSumCost = str_replace(".",",",number_format($cartSumCost,2));

            $formTemplate->cartDetails = $cartDetails;

            $formTemplate->orderformFormData = $objFormDataFromSession;

            $excludeFormdataFields = (array) explode(',', $currentOrderformForm->formdata_exclusion);
            $formTemplate->excludeFormdataFields = $excludeFormdataFields;

            $objForm = new \Haste\Form\Form('znrl_orderform_form_'.$currentOrderformForm->id, 'POST', function($objHaste) {
                return \Input::post('FORM_SUBMIT') === $objHaste->getFormId();
            }, true);


                $objForm->addFieldsFromFormGenerator($currentOrderformForm->form, function(&$strField, &$arrDca) {

                    $arrDca['value'] = $this->arrOrderformSession['formdata'][$this->arrOrderformSession['step']][$strField];

                    return true;
                });


            $objForm->addFormField('fieldset_submit_start', array(
                'label'         => array('Fortfahren',''),
                'inputType'     => 'fieldset',
                'eval'          => array('fsType' => 'fsStart')
            ));
            $objForm->addSubmitFormField('checkout_back', $GLOBALS['TL_LANG']['MSC']['znrl_orderform']['checkout_back']);
            $objForm->addSubmitFormField('purchase_submit', $GLOBALS['TL_LANG']['MSC']['znrl_orderform']['purchase_submit']);
            $objForm->addFormField('fieldset_submit_end', array(
                'inputType'     => 'fieldset',
                'eval'          => array('fsType' => 'fsEnd')
            ));


            if($objForm->validate()) {


                $objForm->fetchAll(function($strName, $objWidget) {
                    if ($objWidget->type != 'submit' && $objWidget->type != 'hidden' && $objWidget->type != 'fieldset' && $objWidget->type != 'explanation') {

                        $this->arrOrderformSession['formdata'][$this->arrOrderformSession['step']][$strName] = $objWidget->value;

                    }
                });

                if (\Input::post('checkout_back') != null) {
                    return 'back';
                }
                elseif (\Input::post('purchase_submit') != null) {

                    //die info wohin weitergeleitet werden soll gibts nich global also muss mans erstmal hier machen...

                    if ($this->sendOrderform($cartDetails, $objFormDataFromSession, $currentOrderformForm,  $excludeFormdataFields)) {
                        unset($this->arrOrderformSession);
                        $this->setSessionData();

                        $objPage =\PageModel::findByPk($currentOrderformForm->redirect_page);
                        $uri = \Controller::generateFrontendUrl($objPage->row());
                        \Controller::redirect($uri);
                    }

                    return 'send_orderform_failed';
                }


            }

        }


        $formTemplate->form = $objForm->generate();
        $objForm->addToTemplate($formTemplate);


        $this->Template->form = $formTemplate->parse();

        if (\Input::post('FORM_SUBMIT') != null) {
            return 'invalid';
        }

    }


    protected function getSessionData()
    {
        $this->arrOrderformSession = \Session::getInstance()->get($this->arrOrderformSessionKey);
        if (!isset($this->arrOrderformSession)) {
            $allForms = $this->getAllOrderformForms();
            $i = 0;
            foreach ($allForms as $form) {
                $arrForm = array(   'id'        => $form->id,
                                    'completed' => false,
                                    'condition' => $form->form_condition,
                                    'skip'      => false);

                if ($form->type == 'cart') {
                    $this->arrOrderformSession['stepCart'] = $i;
                }
                $this->arrOrderformSession['forms'][$i++] = $arrForm;
            }
            $this->arrOrderformSession['step'] = 0;
            $this->arrOrderformSession['stepCount'] = count($allForms);
            $this->arrOrderformSession['formdata'] = '';
        }
    }


    protected function setSessionData()
    {
        \Session::getInstance()->set($this->arrOrderformSessionKey, $this->arrOrderformSession);
    }


    protected function reloadAfterPostGetToSession()
    {
        $this->setSessionData();
        $uri = \Haste\Util\Url::removeQueryString(array('addarticle'));
        \Controller::redirect($uri);
    }


    protected function getOrderformSettings()
    {
       return OrderformModel::findOneBy('id', $this->znrl_orderform);
    }


    protected function getProductDetails($articleId)
    {
        $db = \Database::getInstance();
        $table = $this->orderformSettings->products_table;
        $idCol = $this->orderformSettings->products_id_col;

        $selectCols = $this->orderformSettings->products_id_col.','.$this->orderformSettings->products_title_col.','.$this->orderformSettings->products_price_col;

        if ($this->orderformSettings->products_add_cols == 1) {
            $arrCols = unserialize($this->orderformSettings->products_additional_cols);
            foreach ($arrCols as $col) {
                $selectCols .= ','.$col["products_additional_cols_col"];
            }
        }

        if (is_array($articleId)) {
            $articleIds = implode(",", $articleId);
            $stmt = $db->prepare("SELECT $selectCols FROM $table WHERE $idCol IN ($articleIds)");
            $articleDetails = $stmt->execute(1);
        }
        else {
            $stmt = $db->prepare("SELECT $selectCols FROM $table WHERE $idCol=?");
            $articleDetails = $stmt->execute($articleId, 1);
        }

        return $articleDetails;
    }


    protected function getProductDetailsCollection($articlesInCart)
    {
        foreach (array_keys($articlesInCart) as $key) {
            $arrArticleId[] = substr($key, 9);
        }

        return \Model\Collection::createFromDbResult($this->getProductDetails($arrArticleId), 'tl_znrl_orderform');

    }


    protected function addArticleAfterRedirect($article)
    {
            $articleId = $article;
            $this->arrOrderformSession['addarticle'] = $articleId;
            $this->reloadAfterPostGetToSession();
    }


    protected function addArticleToCart($article)
    {
        $addArticle = $article;
        $articleDetails = $this->getProductDetails($addArticle);

        if($articleDetails->numRows != 0) {
            if (!isset($this->arrOrderformSession['formdata']['cart']['quantity_'.$articleDetails->id])) {
                $this->arrOrderformSession['formdata']['cart']['quantity_'.$articleDetails->id] = '';
            }
            $this->arrOrderformSession['formdata']['cart']['quantity_'.$articleDetails->id] += 1;
        }
        unset($this->arrOrderformSession['addarticle']);
    }


    protected function getAllOrderformForms()
    {
        return OrderformFormsModel::findAll(array('column' => 'pid', 'value' => $this->znrl_orderform, 'order' => 'sorting ASC'));
    }


    protected function getOrderformForm($offset)
    {
        return OrderformFormsModel::findOneBy('pid', $this->znrl_orderform, array('order' => 'sorting ASC', 'offset' => $offset));
    }


    protected function getShippingCoutries($id)
    {
        $arrShippingSettings = OrderformShippingModel::findOneBy('id', $id);
        $shippingOptions = unserialize($arrShippingSettings->shipping_options);
        foreach ($shippingOptions as $option) {
            $countries[] = array(   'value' => $option['country_value'],
                                    'label' => $option['country']);
        }
        return serialize($countries);
    }


    protected function getPreparedFormDataFromSession()
    {
        $formData = $this->arrOrderformSession['formdata'];

        $objOrderforms = $this->getAllOrderformForms();

        $arrFormId = $objOrderforms->fetchEach('form');

        $objGenForms = \FormModel::findMultipleByIds($arrFormId);

        $objFormData = new \stdClass;

        foreach ($formData as $key=>$data) {

            if ($key != 'cart' && $this->arrOrderformSession['forms'][$key]['skip'] != 1) {
                $objData = new \stdClass;
                $objData->step = $key;
                $objData->changeDataHref = \Haste\Util\Url::addQueryString('gotostep='.$objData->step);
                $objData->orderformId = $this->arrOrderformSession['forms'][$objData->step]['id'];

                foreach ($objOrderforms as $orderform) {
                    if ($orderform->id == $objData->orderformId) {
                        $objData->orderformTitle = $orderform->title;
                        $objData->formGenId = $orderform->form;
                        $objData->orderformType = $orderform->type;
                    }
                }
                foreach ($objGenForms as $genForm) {
                    if ($genForm->id == $objData->formGenId) {
                        $objData->formGenTitle = $genForm->title;
                    }
                }
                $objData->formData = (object) $this->arrOrderformSession['formdata'][$objData->step];
                $arrObjData[] = $objData;
            }
        }

        $objFormData = $arrObjData;

        return $objFormData;
    }


    protected function getCartDetails($objFormDataFromSession = null)
    {
        $cartDetails = new \stdClass;

        $arrCart = $this->arrOrderformSession['formdata']['cart'];

        $cartDetails->changeArticlesHref = \Haste\Util\Url::addQueryString('gotostep=cart');

        if (!empty($arrCart)) {
            foreach ($arrCart as $article) {
                $quantity += $article;
            }
            $cartDetails->articleSum = $quantity;
            $cartDetails->cartIsEmpty = 0;
            $cartDetails->cartEmptyText = $GLOBALS['TL_LANG']['MSC']['znrl_orderform']['cart_empty'];
            $cartDetails->productDetails = $this->getProductDetailsCollection($arrCart);
        }
        else {
            $cartDetails->articleSum = 0;
            $cartDetails->cartIsEmpty = 1;
        }


        $cartDetails->taxRate = $this->orderformSettings->tax_rate;
        $cartDetails->taxRateFormatted = $GLOBALS['TL_LANG']['MSC']['znrl_orderform']['inclusive_of']
                                                    .' '.$this->orderformSettings->tax_rate
                                                    .' % '.$GLOBALS['TL_LANG']['MSC']['znrl_orderform']['value_added_tax'];

        $cartDetails->currency = $this->orderformSettings->currency;


        if (isset($objFormDataFromSession)) {

            // data nach country , email und business durchsuchen

            // die decider und form conditions funktionieren vermutl nicht bei feldern mit array

            $businessField = $this->orderformSettings->business_decider_field;
            $businessValue = $this->orderformSettings->business_decider_value;
            $shippingOptionsField = $this->orderformSettings->shipping_options_field;
            $shippingSettings = $this->orderformSettings->shipping_settings;
            $shippingChargesUseGlobal = $this->orderformSettings->shipping_charges_use_global;


            foreach ($objFormDataFromSession as $data) {


                if (isset($data->formData->$businessField) && ($data->formData->$businessField == $businessValue || ($businessValue == '#ANY#' && $data->formData->$businessField != null))) {
                    $cartDetails->isBusinessOrder = 1;
                    $cartDetails->isBusinessOrderText = $GLOBALS['TL_LANG']['MSC']['znrl_orderform']['business'];
                    if ($shippingChargesUseGlobal == 1) {
                        $shippingCharges = $this->orderformSettings->shipping_charges_global_business;
                    }
                }
                elseif (isset($data->formData->$businessField) && $data->formData->$businessField != $businessValue) {
                    $cartDetails->isBusinessOrder = null;
                    $cartDetails->isBusinessOrderText = '';
                    if ($shippingChargesUseGlobal == 1) {
                        $shippingCharges = $this->orderformSettings->shipping_charges_global;
                    }

                }

                if (isset($data->formData->$shippingOptionsField) && $shippingSettings == 'znrl_orderform_shipping') {
                    $shippingCountry = $data->formData->$shippingOptionsField;

                    $arrShippingSettings = OrderformShippingModel::findOneBy('id', $this->orderformSettings->shipping_options);
                    $shippingOptions = unserialize($arrShippingSettings->shipping_options);

                    foreach ($shippingOptions as $option) {
                        if ($option['country_value'] == $shippingCountry) {
                            $cartDetails->shippingCountry = $option['country'];

                            // change country in formdata to use the label but also keep identifiername as original value
                            $cartDetails->shippingCountryOrigValue = $option['country_value'];
                            $data->formData->$shippingOptionsField = $cartDetails->shippingCountry;
                            if ($cartDetails->isBusinessOrder == 1 && $shippingChargesUseGlobal != 1) {
                                $shippingCharges = $option['shipping_charges_business'];

                            }
                            elseif ($cartDetails->isBusinessOrder != 1 && $shippingChargesUseGlobal != 1) {
                                $shippingCharges = $option['shipping_charges'];
                            }

                        }
                    }

                    //mehr möglichkeiten z.b. nichts schicken oder so
                    if (isset($data->formData->email)) {
                        $cartDetails->emailAdress = $data->formData->email;
                    }
                }
            }


            if ($shippingCharges == null) {
                $cartDetails->shippingCharges = null;
                $cartDetails->shippingChargesText = $GLOBALS['TL_LANG']['MSC']['znrl_orderform']['sum_plus'].' '.$GLOBALS['TL_LANG']['MSC']['znrl_orderform']['shipping_charge'];
                if ($cartDetails->isBusinessOrder) {
                    $cartDetails->shippingChargesText .= ' ('.$cartDetails->isBusinessOrderText.')';
                }
            }
            elseif ($shippingCharges > 0) {
                $cartDetails->shippingCharges = str_replace(".",",",number_format($shippingCharges, 2));
                $cartDetails->shippingChargesText = $GLOBALS['TL_LANG']['MSC']['znrl_orderform']['shipping_charge'].' '.$cartDetails->shippingCountry;
                if ($cartDetails->isBusinessOrder) {
                    $cartDetails->shippingChargesText .= ' ('.$cartDetails->isBusinessOrderText.')';
                }
            }
            elseif ($shippingCharges != null && $shippingCharges == 0) {
                $cartDetails->shippingCharges = '';
                $cartDetails->shippingChargesText = $GLOBALS['TL_LANG']['MSC']['znrl_orderform']['shipping_charge_free'];
            }

        }

        return $cartDetails;
    }

    protected function sendOrderform($cart, $formData, $formSettings, $excludeFormdataFields)
    {

        foreach ($cart->productDetails as $product) {

            $mailCart = array();

            $arrProduct = $product->row();

            $mailCartText .= \String::parseSimpleTokens($formSettings->mail_cart_text, $arrProduct)."\r\n\r\n";
        }

        if ($cart->shippingCharges != null) {
            $mailCartText .= "\r\nVersandkosten: ".$cart->shippingChargesText." - "
                            .$cart->shippingCharges." ".$cart->currency."\r\n";
        }

        $mailCartText .= "\r\nGesamtpreis: ".$cart->cartSumCost." ".$cart->currency."\r\n"
                        .$cart->taxRateFormatted."\r\n";

        if ($cart->shippingCharges == null || $cart->shippingCharges == '') {
            $mailCartText .= $cart->shippingChargesText."\r\n";
        }

        $mailFormDataText = "\r\nDaten:\r\n";

        $arrFormData = array();
        foreach ($formData as $form) {
            if ($form->orderformType != "confirmation") {
                $mailFormDataText .= "\r\n".$form->formGenTitle.":\r\n";
            }
            foreach ($form->formData as $key=>$data) {
                if (!empty($data) && !is_array($data) && !in_array($key, $excludeFormdataFields)) {
                    $mailFormDataText .= $data."\r\n";
                }
                $arrFormData[$form->orderformId.'_'.$key] = $data;
            }

        }



        $arrMailContent = array('mail_cart_text' => $mailCartText,
                                'mail_form_data_text' => $mailFormDataText);

       array_insert($arrMailContent, 0, $arrFormData);

        //auch nich ganz praktisch hier alles in einem und empfänger/sender des selbe replyto...
        $objEmail  = new \Email();

        $objEmail->from = $formSettings->mail_recipient;
        $objEmail->subject = $formSettings->mail_subject;
        $objEmail->text = \String::parseSimpleTokens($formSettings->mail_text, $arrMailContent);
        if (!empty($cart->emailAdress)) {
            $objEmail->sendTo($cart->emailAdress);
        }

        if ($objEmail->sendTo($formSettings->mail_recipient)) {
            return true;
        }


    //return true und redirect, unset -> setsession session
    }

}