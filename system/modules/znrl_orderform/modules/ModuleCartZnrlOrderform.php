<?php

/**
 * Contao Open Source CMS
*
* Copyright (c) 2005-2014 Leo Feyer
*
* @package   ZnrlOrderform
* @author    Lorenz Ketterer <lorenz.ketterer@web.de>
* @license   GNU/LGPL
* @copyright Lorenz Ketterer 2015
*/


/**
 * Namespace
*/

namespace Znrl\ZnrlOrderform;
use Contao;


/**
 * Module CartZnrlOrderform
 *
 * @copyright  Lorenz Ketterer 2015
 * @author     Lorenz Ketterer <lorenz.ketterer@web.de>
 */

class ModuleCartZnrlOrderform extends \Module
{

    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'mod_cart_znrl_orderform';

    protected $arrOrderformSession = array();

    protected $arrOrderformSessionKey;


    /**
     * Display a wildcard in the back end
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new \BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### ' . utf8_strtoupper($GLOBALS['TL_LANG']['FMD']['cart_znrl_orderform'][0]) . ' ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }

        return parent::generate();
    }


    /**
     * Generate the module
     */
    protected function compile()
    {
        $this->arrOrderformSessionKey = 'znrl_orderform_'.$this->znrl_orderform;
        $this->getSessionData();

        if (!isset($this->arrOrderformSession['formdata']['cart']) || $this->arrOrderformSession['formdata']['cart'] == 0) {
            $countArticlesInCart = 0;
        }
        else {
            $arrCart = $this->arrOrderformSession['formdata']['cart'];
            foreach ($arrCart as $articleQuantity) {
                $countArticlesInCart += $articleQuantity;
            }
        }

        // not the best solution
        if (isset($this->arrOrderformSession['addarticle'])) {
            $countArticlesInCart += 1;
        }
        $this->Template->countArticlesInCart = $countArticlesInCart;

        $cartImgPath = 'system/modules/znrl_orderform/assets/cart.png';
        $cartImgWidth = '30';
        $cartImgHeight = '23';
        $cartImgAltText = '';
        $src = \Image::get($cartImgPath, $cartImgWidth, $cartImgHeight, 'center_center');
        $this->Template->cartImg = \Image::getHtml($src, $alt = $cartImgAltText, $attributes = 'class="znrl_orderform_cart_img"');

        if ($this->cart_znrl_orderform_show_always != 1 && $countArticlesInCart == 0) {
            $this->Template->hideCart = true;
        }

        $cartPage = $this->znrl_orderform_page;
        $this->Template->linkCart = \Haste\Util\Url::addQueryString('gotostep=cart', $cartPage);
    }


    protected function getSessionData()
    {
        $this->arrOrderformSession = \Session::getInstance()->get($this->arrOrderformSessionKey);
    }
}